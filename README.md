# A flight monitor

## History and context

I'm living in a town close to an international airport: Villecresnes, at around 10 km from the ORLY airport (Paris, France area).

I'm in a house and we often heard planes coming from or arriving at the airport.

I had the feeling there were *a lot* of planes, but no figures at all.

After tsome investigations, I decided to start data collection of planes passing over my town, to have facts instead of feeling.


## Project's requirement: hardware

My instance of this project is using the following hardware:
-   Raspebbry Pi Zero (inc. case, to protect it)
-   Wifi USB dongle, basic one, to be network connected
-   An USB 2.0 TV Stick DVB-T, like this one: [http://amzn.eu/dp89lgA](http://amzn.eu/dp89lgA) (12.99€)
-   A µSD card (I used a 16Gb, but a 8Gb is more than enough!)
-   Of course, a power cord (usb micro -> wallplug)


It is obvious a **Raspberry Pi Zero W** can be used, in replacement of the Rapsberry Pi Zero and the Wifi dongle.

You could also use any other Raspberry Pi or any Linux computer, but the cost is not the same!

All this documentation will only mention the Rpi platform: installing some components on Linux may differs (RPi is ARM, your Linux computer not). Please adjust by yourself.

## Twitter account to follow

The application is tweeting on 2 different accounts: @airvillecresnes and @aero_stats

Do not hesitate to follow them ! :)

## Important note regarding Departure and Arrival airports

In the current version, tweets are mentioning the departure airport and the arrival airport, and their respective country name (in french).

This has been made possible thanks to the **Plane Finder** agent! This agent collect airplanes data and send it to a central platform, accessible to this URL: [https://planefinder.net/](https://planefinder.net/).

But it also bring a local API/URL that let me get this kind of data:
```javascript
{
	"user": {
		"user_lat": "xxxx",
		"user_lon": "xxxx"
	},
	"aircraft": {
		"4CAB9A": {
			"callsign": "RYR64WJ",
			"altitude": 34000,
			"heading": 70,
			"vert_rate": 0,
			"speed": 389,
			"lat": 48.870117,
			"lon": 2.741018,
			"polar_distance": 11.740123,
			"polar_bearing": 41.782267,
			"is_adsb": true,
			"squawk": "2333",
			"reg": "EI-FTO",
			"flightno": "FR5054",
			"route": "MAD-LUX",
			"type": "B738",
			"category": "A3",
			"is_on_ground": false,
			"last_seen_time": 1519570572,
			"pos_update_time": 1519570433
		},
    .....
```

As you can notice, there is the real flight number (and not the callsign I tweeted) and the ROUTE (here MAD-LUX in this case).

Then, I retrive the full list of world airports on [OpenFlights Data](https://openflights.org/data.html), translated it in my DB, and .... WOSH ! The magic happened! :)


Why do I explain that? Because, if you want to reproduce the same system, you will also have to register on PlaneFinder and install the agent. This part is NOT documented here, as the PlaneFinder doc is well done.




# Installations

## Raspberry Pi preparation

1.  Install your Raspberry Pi using the standard Debian distribution, like describe on the Raspberry Fundation pages ... or many others.

1.  Connect your RPi to the network (wifi)

1.  Assign a fixed IP address to your Raspberry Pi, it would be easier to connect to. To do so, you will have to play with the ```/etc/dhcpcd.conf``` file. Please refer to RPi docs.

1.  As every fresh install, update packages: ```sudo apt-get update && sudo apt-get upgrade```


## Flights probe: DVB-T

This sitck, originally used to get TV on your computer, will be used to scan flights around you.

To do so, there are few components to install. A script is available in this repository. But first, you will have to clone this repo on your PI.

1.  Physically plug the USB stick
1.  Install git: ```sudo apt-get update && sudo apt-get install git-core git```
1.  Create the ```sources``` directory: ```mkdir ~/sources && cd ~/sources```
1.  Clone the project repository: ```git clone https://gitlab.com/Nekloth/zgur-flight-monitor.git```
1.  Make the file runable: ```chmod +x ~/sources/zgur-flight-monitor/install_probe.sh```
1.  Install the dependancies components: ```sudo ~/sources/zgur-flight-monitor/install_probe.sh```


### Debug
If you need to debug the install script (it should work but ... we never knows), please refer to the website mentionned at the end of this document.

## Daemon installation

The key component that will deliver the raw data is the wonderful component **dump1090**. To make our system automatic, we need to make it run *always*, to make it *daemon*.

1.  Copy the daemon file in the appropriate directory: ```sudo cp ~/sources/zgur-flight-monitor/dump1090.sh /etc/init.d/dump1090.sh```
1.  Make the file runnable: ```sudo chmod +x /etc/init.d/dump1090.sh```
1.  Update dameon list: ```sudo update-rc.d dump1090.sh defaults```
1.  Stop the service: ```sudo /etc/init.d/dump1090.sh stop```
1.  Start the service: ```sudo /etc/init.d/dump1090.sh start```


To test if it worked, open a web browser (Chromium) on your Raspberry Pi and open the URL ```http://localhost:8080/data.json```.

You should see something like:
```
[
{"hex":"4b1887", "squawk":"2036", "flight":"SWR9K   ", "lat":48.599487, "lon":2.700594, "validposition":1, "altitude":39000,  "vert_rate":64,"track":126, "validtrack":1,"speed":456, "messages":374, "seen":0},
{"hex":"39e4ac", "squawk":"1000", "flight":"CCM771F ", "lat":48.744186, "lon":2.569744, "validposition":1, "altitude":4825,  "vert_rate":1792,"track":86, "validtrack":1,"speed":256, "messages":219, "seen":2},
{"hex":"400935", "squawk":"2725", "flight":"BAW564Y ", "lat":0.000000, "lon":0.000000, "validposition":0, "altitude":36975,  "vert_rate":0,"track":156, "validtrack":1,"speed":428, "messages":61, "seen":2},
{"hex":"39b9e0", "squawk":"1000", "flight":"AFR7382 ", "lat":48.670120, "lon":2.524813, "validposition":1, "altitude":4575,  "vert_rate":1280,"track":176, "validtrack":1,"speed":188, "messages":615, "seen":93},
{"hex":"4b1884", "squawk":"2047", "flight":"SWR15X  ", "lat":48.552913, "lon":2.797611, "validposition":1, "altitude":39000,  "vert_rate":-64,"track":98, "validtrack":1,"speed":473, "messages":204, "seen":49},
{"hex":"abbee3", "squawk":"0000", "flight":"", "lat":0.000000, "lon":0.000000, "validposition":0, "altitude":12850,  "vert_rate":0,"track":0, "validtrack":0,"speed":0, "messages":18, "seen":214},
{"hex":"3985a5", "squawk":"1000", "flight":"AFR85DB ", "lat":48.636056, "lon":2.627756, "validposition":1, "altitude":12000,  "vert_rate":1280,"track":168, "validtrack":1,"speed":265, "messages":868, "seen":59},
{"hex":"3c085a", "squawk":"1125", "flight":"SDR3012 ", "lat":0.000000, "lon":0.000000, "validposition":0, "altitude":37000,  "vert_rate":0,"track":239, "validtrack":1,"speed":421, "messages":111, "seen":186},
{"hex":"4ac961", "squawk":"0705", "flight":"", "lat":0.000000, "lon":0.000000, "validposition":0, "altitude":33000,  "vert_rate":0,"track":200, "validtrack":1,"speed":416, "messages":141, "seen":214},
{"hex":"4010ee", "squawk":"1000", "flight":"EZY392N ", "lat":48.645493, "lon":2.578407, "validposition":1, "altitude":11400,  "vert_rate":1472,"track":212, "validtrack":1,"speed":286, "messages":866, "seen":138},
{"hex":"44f427", "squawk":"7140", "flight":"BAF667  ", "lat":0.000000, "lon":0.000000, "validposition":0, "altitude":27000,  "vert_rate":0,"track":0, "validtrack":0,"speed":0, "messages":115, "seen":214},
{"hex":"4ca737", "squawk":"2316", "flight":"RYR3ZX  ", "lat":48.835739, "lon":2.526010, "validposition":1, "altitude":36000,  "vert_rate":0,"track":70, "validtrack":1,"speed":479, "messages":110, "seen":284},
{"hex":"3985a0", "squawk":"1000", "flight":"AFR59EG ", "lat":48.661560, "lon":2.543687, "validposition":1, "altitude":10225,  "vert_rate":2880,"track":225, "validtrack":1,"speed":280, "messages":772, "seen":250},
{"hex":"3985a2", "squawk":"1000", "flight":"AFR33ZU ", "lat":48.631348, "lon":2.550307, "validposition":1, "altitude":8525,  "vert_rate":1216,"track":214, "validtrack":1,"speed":297, "messages":1040, "seen":285}
]
```

If yes, you can go on !

If no ... good luck to degbu by yourself! :P

## Database installation

My implementation of the project is *"distributed"* in the sense I decided to not install the database on the Raspberry Pi.

I installed a MySQL server on my NAS (Synology) which is on 24/7/365.

It's up to you to decide to install the database on the Rapsberry Pi, but take into account memory: a RPi zero does not comes with a large amount of memory!

### Table creation scripts

The SQL script for tables creation is ```~/sources/zgur-flight-monitor/sql_queries/00_table_creation.sql```.

## Node-red flow installation

Normally, Node-red is pre-installed on the Raspbian distribution.

To run it as a daemon, enter the command: ```sudo systemctl enable nodered.service```.

To see the Node-red logs, enter ```node-red-log```.

## Node-red flow configuration

This project uses 2 additional nodes, one to connect to MySQL, one to have ```cron``` capabilities.

To install these dependancies, please follow these steps:
1.  ```cp ~/sources/zgur-flight-monitor/node-red_items/package.json ~/.node-red/package.json```
1.  ```cd ~ && npm install```


## Tricky point: install flows

Normally, in the ```~/.node-red```, you should have a JSON file name ```flows_<hostname>.json```, where ```<hostname>``` is the name of your Raspberry Pi (if you did not change it, it should be ```raspberry```).

This file contains the Node-Red flows. Empty on an fresh install. We will replace it:
1.  ```rm ~/.node-red/flows_<hostname>.json```
1.  ```cp ~/sources/zgur-flight-monitor/node-red_items/flows_raspberry.json ~/.node-red/flows_<hostname>.json```


## Node-Red credentials

At this stage, the node-red flow will not work because you did not setup the credentials to use to:
1.  Connect to MySQL
1.  To tweet with your account

To set it up, open your web browser on the URL ```http://localhost:1880``` if you do this on the Rapsberry Pi or ```http://<rpi_ip_address>:1880``` otherwise.

Look for the tab called *ZFM / Watch*.

Double-click on the node called *aerostuff*
![MySQL](./doc_assets/db_node.png)

... and edit the database connection credentials
![MySQL](./doc_assets/mysql_conf_1.png)
![MySQL](./doc_assets/mysql_conf_2.png)


Double-click on the Twitter node called *@AirVillecresnes*
![MySQL](./doc_assets/twitter_node.png)

... and edit the Twitter connection credentials
![MySQL](./doc_assets/twitter_conf.png)


***AND THAT'S ALL FOLKS !***

Believe me, it is very hard to make this installation as simple as a script run. I have to admit this still is very manual. Sorry about that.

May you experience any troubles, you will have to figure out why by your own, to dig in this doc an dmany other you will find on internet.

# Flows screenshots

The ~realtime flow
![Realtime](./doc_assets/realtime_watch.png)


The daily flow
![Daily](./doc_assets/Daily.png)

The monthly flow
![Monthly](./doc_assets/Monthly.png)



# Information and contact

Feel free to contact me ( nekloht.at.zgur.net ) and to fork this project! Any enhancement or feature requests are welcome too!

## Some links

*   [http://www.satsignal.eu/raspberry-pi/dump1090.html](http://www.satsignal.eu/raspberry-pi/dump1090.html)
