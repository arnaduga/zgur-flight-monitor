SET @limit_altitude=8000;

# Définition des bornes de date
SELECT @date_beg := DATE(DATE_ADD(now(),INTERVAL -9 DAY)) ;
SELECT @date_end := DATE(DATE_ADD(now(),INTERVAL -3 DAY)) ;

SELECT WEEK(@date_beg), @date_beg,  @date_end;

# Nombre d'avions total
SELECT count(*) as week_number_of_planes
FROM aerostuff.flights_villecresnes 
WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) >= @date_beg AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) <= @date_end;

# Nombde et moyenne d'altitude d'avions hauts 
SELECT count(*) as week_number_of_high_planes , ROUND(avg(altitude)) week_avg_high
FROM aerostuff.flights_villecresnes 
WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) >= @date_beg AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) <= @date_end
AND altitude >= @limit_altitude;

# Nombre et moyene d'altitude d'avions bas
SELECT count(*) as week_number_of_low_planes , ROUND(avg(altitude)) week_avg_low
FROM aerostuff.flights_villecresnes 
WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) >= @date_beg AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) <= @date_end
AND altitude < @limit_altitude;


# Première heure de passage
SELECT TIME_FORMAT(MIN(pretty_time),"%H:%i")
FROM aerostuff.flights_villecresnes 
WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) >= @date_beg AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) <= @date_end
AND altitude < @limit_altitude;

# Dernière heure de passage
SELECT TIME_FORMAT(MAX(pretty_time),"%H:%i")
FROM aerostuff.flights_villecresnes 
WHERE DATE(FROM_UNIXTIME(LEFT(seen_at,10))) >= @date_beg AND DATE(FROM_UNIXTIME(LEFT(seen_at,10))) <= @date_end
AND altitude < @limit_altitude;
    

    
    
    
