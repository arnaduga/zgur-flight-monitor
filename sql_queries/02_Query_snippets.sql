﻿/*	**
	* Query snippets
	*
	* Date: 2016.11.22
	** */
	
SELECT 
	(
		SELECT count(*) 
		FROM aerostuff.flights_villecresnes
		WHERE 
			WEEK(FROM_UNIXTIME(seen_at)) < WEEK(NOW())-1  
			AND altitude < 10000 
	) as number_of_low_planes,
	count(*) number_of_planes, 
	avg(altitude) altitude_average,
	MIN(TIME(FROM_UNIXTIME(seen_at))) seen_between_start,
	MAX(TIME(FROM_UNIXTIME(seen_at))) seen_between_end,
	FROM_UNIXTIME(seen_at), 
	WEEK(FROM_UNIXTIME(seen_at)) seen_week_number,
	DAYOFWEEK(FROM_UNIXTIME(seen_at)) seen_day_of_week, 
	DAYOFMONTH(FROM_UNIXTIME(seen_at)) seen_day_of_month, 
	MONTH(FROM_UNIXTIME(seen_at)) seen_month,
	YEAR(FROM_UNIXTIME(seen_at)) seen_year
FROM aerostuff.flights_villecresnes



#WHERE 
#	WEEK(FROM_UNIXTIME(seen_at)) < WEEK(NOW())-1
#	AND distance_from_villecresnes <= 3
